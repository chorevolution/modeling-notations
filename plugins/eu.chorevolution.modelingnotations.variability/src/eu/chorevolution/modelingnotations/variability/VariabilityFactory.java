/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage
 * @generated
 */
public interface VariabilityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VariabilityFactory eINSTANCE = eu.chorevolution.modelingnotations.variability.impl.VariabilityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Variation Point Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variation Point Model</em>'.
	 * @generated
	 */
	VariationPointModel createVariationPointModel();

	/**
	 * Returns a new object of class '<em>Choreography</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Choreography</em>'.
	 * @generated
	 */
	Choreography createChoreography();

	/**
	 * Returns a new object of class '<em>Call Choreography</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Choreography</em>'.
	 * @generated
	 */
	CallChoreography createCallChoreography();

	/**
	 * Returns a new object of class '<em>Call Choreography Variant Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Choreography Variant Point</em>'.
	 * @generated
	 */
	CallChoreographyVariantPoint createCallChoreographyVariantPoint();

	/**
	 * Returns a new object of class '<em>Choreography Variant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Choreography Variant</em>'.
	 * @generated
	 */
	ChoreographyVariant createChoreographyVariant();

	/**
	 * Returns a new object of class '<em>Choreography Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Choreography Context</em>'.
	 * @generated
	 */
	ChoreographyContext createChoreographyContext();

	/**
	 * Returns a new object of class '<em>Context Evaluator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Context Evaluator</em>'.
	 * @generated
	 */
	ContextEvaluator createContextEvaluator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VariabilityPackage getVariabilityPackage();

} //VariabilityFactory
