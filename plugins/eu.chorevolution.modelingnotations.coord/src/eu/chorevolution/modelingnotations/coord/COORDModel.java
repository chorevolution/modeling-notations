/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COORD Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getName <em>Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getChoreographyName <em>Choreography Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getTuples <em>Tuples</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getStates <em>States</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getOperations <em>Operations</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getParticipants <em>Participants</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.COORDModel#getInitialState <em>Initial State</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel()
 * @model
 * @generated
 */
public interface COORDModel extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Choreography Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choreography Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choreography Name</em>' attribute.
	 * @see #setChoreographyName(String)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_ChoreographyName()
	 * @model required="true"
	 * @generated
	 */
	String getChoreographyName();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getChoreographyName <em>Choreography Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choreography Name</em>' attribute.
	 * @see #getChoreographyName()
	 * @generated
	 */
	void setChoreographyName(String value);

	/**
	 * Returns the value of the '<em><b>Tuples</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.coord.Tuple}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tuples</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tuples</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_Tuples()
	 * @model containment="true"
	 * @generated
	 */
	EList<Tuple> getTuples();

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.coord.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.coord.AllowedOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<AllowedOperation> getOperations();

	/**
	 * Returns the value of the '<em><b>Participants</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.coord.Participant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participants</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participants</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_Participants()
	 * @model containment="true"
	 * @generated
	 */
	EList<Participant> getParticipants();

	/**
	 * Returns the value of the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial State</em>' reference.
	 * @see #setInitialState(State)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getCOORDModel_InitialState()
	 * @model required="true"
	 * @generated
	 */
	State getInitialState();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getInitialState <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial State</em>' reference.
	 * @see #getInitialState()
	 * @generated
	 */
	void setInitialState(State value);

} // COORDModel
