/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>COAP Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getCOAPModel()
 * @model
 * @generated
 */
public interface COAPModel extends InterfaceDescription {
} // COAPModel
