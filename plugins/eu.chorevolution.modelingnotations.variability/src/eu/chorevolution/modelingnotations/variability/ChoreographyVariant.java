/**
 */
package eu.chorevolution.modelingnotations.variability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choreography Variant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#getChoreographyContext <em>Choreography Context</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getChoreographyVariant()
 * @model
 * @generated
 */
public interface ChoreographyVariant extends Choreography {
	/**
	 * Returns the value of the '<em><b>Is Active</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Active</em>' attribute.
	 * @see #setIsActive(boolean)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getChoreographyVariant_IsActive()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIsActive();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#isIsActive <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Active</em>' attribute.
	 * @see #isIsActive()
	 * @generated
	 */
	void setIsActive(boolean value);

	/**
	 * Returns the value of the '<em><b>Choreography Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choreography Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choreography Context</em>' reference.
	 * @see #setChoreographyContext(ChoreographyContext)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getChoreographyVariant_ChoreographyContext()
	 * @model required="true"
	 * @generated
	 */
	ChoreographyContext getChoreographyContext();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#getChoreographyContext <em>Choreography Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choreography Context</em>' reference.
	 * @see #getChoreographyContext()
	 * @generated
	 */
	void setChoreographyContext(ChoreographyContext value);

} // ChoreographyVariant
