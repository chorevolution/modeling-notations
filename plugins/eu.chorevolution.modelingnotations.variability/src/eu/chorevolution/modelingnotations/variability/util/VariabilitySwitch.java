/**
 */
package eu.chorevolution.modelingnotations.variability.util;

import eu.chorevolution.modelingnotations.variability.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage
 * @generated
 */
public class VariabilitySwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VariabilityPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariabilitySwitch() {
		if (modelPackage == null) {
			modelPackage = VariabilityPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VariabilityPackage.VARIATION_POINT_MODEL: {
				VariationPointModel variationPointModel = (VariationPointModel)theEObject;
				T result = caseVariationPointModel(variationPointModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CHOREOGRAPHY: {
				Choreography choreography = (Choreography)theEObject;
				T result = caseChoreography(choreography);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CALL_CHOREOGRAPHY: {
				CallChoreography callChoreography = (CallChoreography)theEObject;
				T result = caseCallChoreography(callChoreography);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT: {
				CallChoreographyVariantPoint callChoreographyVariantPoint = (CallChoreographyVariantPoint)theEObject;
				T result = caseCallChoreographyVariantPoint(callChoreographyVariantPoint);
				if (result == null) result = caseCallChoreography(callChoreographyVariantPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CHOREOGRAPHY_VARIANT: {
				ChoreographyVariant choreographyVariant = (ChoreographyVariant)theEObject;
				T result = caseChoreographyVariant(choreographyVariant);
				if (result == null) result = caseChoreography(choreographyVariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CHOREOGRAPHY_CONTEXT: {
				ChoreographyContext choreographyContext = (ChoreographyContext)theEObject;
				T result = caseChoreographyContext(choreographyContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VariabilityPackage.CONTEXT_EVALUATOR: {
				ContextEvaluator contextEvaluator = (ContextEvaluator)theEObject;
				T result = caseContextEvaluator(contextEvaluator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variation Point Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variation Point Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariationPointModel(VariationPointModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Choreography</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Choreography</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChoreography(Choreography object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Choreography</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Choreography</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallChoreography(CallChoreography object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Choreography Variant Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Choreography Variant Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallChoreographyVariantPoint(CallChoreographyVariantPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Choreography Variant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Choreography Variant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChoreographyVariant(ChoreographyVariant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Choreography Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Choreography Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChoreographyContext(ChoreographyContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Context Evaluator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context Evaluator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContextEvaluator(ContextEvaluator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //VariabilitySwitch
