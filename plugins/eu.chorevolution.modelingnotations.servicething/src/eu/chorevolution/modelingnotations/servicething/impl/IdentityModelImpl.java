/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.IdentityModel;
import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Identity Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IdentityModelImpl extends IdentityDescriptionImpl implements IdentityModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IdentityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.IDENTITY_MODEL;
	}

} //IdentityModelImpl
