/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.InteractionProtocolDescription;
import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Protocol Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class InteractionProtocolDescriptionImpl extends MinimalEObjectImpl.Container implements InteractionProtocolDescription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionProtocolDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.INTERACTION_PROTOCOL_DESCRIPTION;
	}

} //InteractionProtocolDescriptionImpl
