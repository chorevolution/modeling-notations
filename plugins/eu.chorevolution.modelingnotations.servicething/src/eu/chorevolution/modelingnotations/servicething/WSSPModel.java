/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WSSP Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getWSSPModel()
 * @model
 * @generated
 */
public interface WSSPModel extends SecurityDescription {
} // WSSPModel
