/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WADL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getWADLModel()
 * @model
 * @generated
 */
public interface WADLModel extends InterfaceDescription {
} // WADLModel
