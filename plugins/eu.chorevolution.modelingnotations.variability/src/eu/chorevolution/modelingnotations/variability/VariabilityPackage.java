/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see eu.chorevolution.modelingnotations.variability.VariabilityFactory
 * @model kind="package"
 * @generated
 */
public interface VariabilityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "variability";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://eu.chorevolution/modelingnotations/variability";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "variability";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VariabilityPackage eINSTANCE = eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl.init();

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl <em>Variation Point Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getVariationPointModel()
	 * @generated
	 */
	int VARIATION_POINT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Call Choreograpies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES = 0;

	/**
	 * The feature id for the '<em><b>Choreography</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_POINT_MODEL__CHOREOGRAPHY = 1;

	/**
	 * The number of structural features of the '<em>Variation Point Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_POINT_MODEL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variation Point Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_POINT_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyImpl <em>Choreography</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreography()
	 * @generated
	 */
	int CHOREOGRAPHY = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY__NAME = 0;

	/**
	 * The number of structural features of the '<em>Choreography</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Choreography</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyImpl <em>Call Choreography</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.CallChoreographyImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getCallChoreography()
	 * @generated
	 */
	int CALL_CHOREOGRAPHY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Called Choreograpies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY__CALLED_CHOREOGRAPIES = 1;

	/**
	 * The number of structural features of the '<em>Call Choreography</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Call Choreography</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl <em>Call Choreography Variant Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getCallChoreographyVariantPoint()
	 * @generated
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT__NAME = CALL_CHOREOGRAPHY__NAME;

	/**
	 * The feature id for the '<em><b>Called Choreograpies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT__CALLED_CHOREOGRAPIES = CALL_CHOREOGRAPHY__CALLED_CHOREOGRAPIES;

	/**
	 * The feature id for the '<em><b>Context</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT = CALL_CHOREOGRAPHY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Choreography Variants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS = CALL_CHOREOGRAPHY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Context Evaluator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR = CALL_CHOREOGRAPHY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Call Choreography Variant Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT_FEATURE_COUNT = CALL_CHOREOGRAPHY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Call Choreography Variant Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_CHOREOGRAPHY_VARIANT_POINT_OPERATION_COUNT = CALL_CHOREOGRAPHY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl <em>Choreography Variant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreographyVariant()
	 * @generated
	 */
	int CHOREOGRAPHY_VARIANT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_VARIANT__NAME = CHOREOGRAPHY__NAME;

	/**
	 * The feature id for the '<em><b>Is Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_VARIANT__IS_ACTIVE = CHOREOGRAPHY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Choreography Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT = CHOREOGRAPHY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Choreography Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_VARIANT_FEATURE_COUNT = CHOREOGRAPHY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Choreography Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_VARIANT_OPERATION_COUNT = CHOREOGRAPHY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyContextImpl <em>Choreography Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyContextImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreographyContext()
	 * @generated
	 */
	int CHOREOGRAPHY_CONTEXT = 5;

	/**
	 * The number of structural features of the '<em>Choreography Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_CONTEXT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Choreography Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHOREOGRAPHY_CONTEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.variability.impl.ContextEvaluatorImpl <em>Context Evaluator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.variability.impl.ContextEvaluatorImpl
	 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getContextEvaluator()
	 * @generated
	 */
	int CONTEXT_EVALUATOR = 6;

	/**
	 * The number of structural features of the '<em>Context Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_EVALUATOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Context Evaluator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_EVALUATOR_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.VariationPointModel <em>Variation Point Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variation Point Model</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.VariationPointModel
	 * @generated
	 */
	EClass getVariationPointModel();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.variability.VariationPointModel#getCallChoreograpies <em>Call Choreograpies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Call Choreograpies</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.VariationPointModel#getCallChoreograpies()
	 * @see #getVariationPointModel()
	 * @generated
	 */
	EReference getVariationPointModel_CallChoreograpies();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.variability.VariationPointModel#getChoreography <em>Choreography</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Choreography</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.VariationPointModel#getChoreography()
	 * @see #getVariationPointModel()
	 * @generated
	 */
	EReference getVariationPointModel_Choreography();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.Choreography <em>Choreography</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choreography</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.Choreography
	 * @generated
	 */
	EClass getChoreography();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.variability.Choreography#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.Choreography#getName()
	 * @see #getChoreography()
	 * @generated
	 */
	EAttribute getChoreography_Name();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.CallChoreography <em>Call Choreography</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Choreography</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreography
	 * @generated
	 */
	EClass getCallChoreography();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.variability.CallChoreography#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreography#getName()
	 * @see #getCallChoreography()
	 * @generated
	 */
	EAttribute getCallChoreography_Name();

	/**
	 * Returns the meta object for the reference list '{@link eu.chorevolution.modelingnotations.variability.CallChoreography#getCalledChoreograpies <em>Called Choreograpies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Called Choreograpies</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreography#getCalledChoreograpies()
	 * @see #getCallChoreography()
	 * @generated
	 */
	EReference getCallChoreography_CalledChoreograpies();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint <em>Call Choreography Variant Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Choreography Variant Point</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint
	 * @generated
	 */
	EClass getCallChoreographyVariantPoint();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContext()
	 * @see #getCallChoreographyVariantPoint()
	 * @generated
	 */
	EAttribute getCallChoreographyVariantPoint_Context();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getChoreographyVariants <em>Choreography Variants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Choreography Variants</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getChoreographyVariants()
	 * @see #getCallChoreographyVariantPoint()
	 * @generated
	 */
	EReference getCallChoreographyVariantPoint_ChoreographyVariants();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContextEvaluator <em>Context Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context Evaluator</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContextEvaluator()
	 * @see #getCallChoreographyVariantPoint()
	 * @generated
	 */
	EReference getCallChoreographyVariantPoint_ContextEvaluator();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant <em>Choreography Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choreography Variant</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.ChoreographyVariant
	 * @generated
	 */
	EClass getChoreographyVariant();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#isIsActive <em>Is Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Active</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.ChoreographyVariant#isIsActive()
	 * @see #getChoreographyVariant()
	 * @generated
	 */
	EAttribute getChoreographyVariant_IsActive();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant#getChoreographyContext <em>Choreography Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Choreography Context</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.ChoreographyVariant#getChoreographyContext()
	 * @see #getChoreographyVariant()
	 * @generated
	 */
	EReference getChoreographyVariant_ChoreographyContext();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.ChoreographyContext <em>Choreography Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Choreography Context</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.ChoreographyContext
	 * @generated
	 */
	EClass getChoreographyContext();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.variability.ContextEvaluator <em>Context Evaluator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Context Evaluator</em>'.
	 * @see eu.chorevolution.modelingnotations.variability.ContextEvaluator
	 * @generated
	 */
	EClass getContextEvaluator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VariabilityFactory getVariabilityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl <em>Variation Point Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getVariationPointModel()
		 * @generated
		 */
		EClass VARIATION_POINT_MODEL = eINSTANCE.getVariationPointModel();

		/**
		 * The meta object literal for the '<em><b>Call Choreograpies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES = eINSTANCE.getVariationPointModel_CallChoreograpies();

		/**
		 * The meta object literal for the '<em><b>Choreography</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIATION_POINT_MODEL__CHOREOGRAPHY = eINSTANCE.getVariationPointModel_Choreography();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyImpl <em>Choreography</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreography()
		 * @generated
		 */
		EClass CHOREOGRAPHY = eINSTANCE.getChoreography();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOREOGRAPHY__NAME = eINSTANCE.getChoreography_Name();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyImpl <em>Call Choreography</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.CallChoreographyImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getCallChoreography()
		 * @generated
		 */
		EClass CALL_CHOREOGRAPHY = eINSTANCE.getCallChoreography();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_CHOREOGRAPHY__NAME = eINSTANCE.getCallChoreography_Name();

		/**
		 * The meta object literal for the '<em><b>Called Choreograpies</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_CHOREOGRAPHY__CALLED_CHOREOGRAPIES = eINSTANCE.getCallChoreography_CalledChoreograpies();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl <em>Call Choreography Variant Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getCallChoreographyVariantPoint()
		 * @generated
		 */
		EClass CALL_CHOREOGRAPHY_VARIANT_POINT = eINSTANCE.getCallChoreographyVariantPoint();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT = eINSTANCE.getCallChoreographyVariantPoint_Context();

		/**
		 * The meta object literal for the '<em><b>Choreography Variants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS = eINSTANCE.getCallChoreographyVariantPoint_ChoreographyVariants();

		/**
		 * The meta object literal for the '<em><b>Context Evaluator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR = eINSTANCE.getCallChoreographyVariantPoint_ContextEvaluator();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl <em>Choreography Variant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreographyVariant()
		 * @generated
		 */
		EClass CHOREOGRAPHY_VARIANT = eINSTANCE.getChoreographyVariant();

		/**
		 * The meta object literal for the '<em><b>Is Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHOREOGRAPHY_VARIANT__IS_ACTIVE = eINSTANCE.getChoreographyVariant_IsActive();

		/**
		 * The meta object literal for the '<em><b>Choreography Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT = eINSTANCE.getChoreographyVariant_ChoreographyContext();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyContextImpl <em>Choreography Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.ChoreographyContextImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getChoreographyContext()
		 * @generated
		 */
		EClass CHOREOGRAPHY_CONTEXT = eINSTANCE.getChoreographyContext();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.variability.impl.ContextEvaluatorImpl <em>Context Evaluator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.variability.impl.ContextEvaluatorImpl
		 * @see eu.chorevolution.modelingnotations.variability.impl.VariabilityPackageImpl#getContextEvaluator()
		 * @generated
		 */
		EClass CONTEXT_EVALUATOR = eINSTANCE.getContextEvaluator();

	}

} //VariabilityPackage
