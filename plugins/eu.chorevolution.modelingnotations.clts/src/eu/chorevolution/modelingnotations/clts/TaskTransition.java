/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.clts;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getTaskName <em>Task Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getInMessage <em>In Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getOutMessage <em>Out Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getInitiatingParticipant <em>Initiating Participant</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getReceivingParticipant <em>Receiving Participant</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition()
 * @model
 * @generated
 */
public interface TaskTransition extends Transition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * Returns the value of the '<em><b>Task Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Task Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Task Name</em>' attribute.
	 * @see #setTaskName(String)
	 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition_TaskName()
	 * @model required="true"
	 * @generated
	 */
	String getTaskName();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getTaskName <em>Task Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Task Name</em>' attribute.
	 * @see #getTaskName()
	 * @generated
	 */
	void setTaskName(String value);

	/**
	 * Returns the value of the '<em><b>In Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Message</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Message</em>' containment reference.
	 * @see #setInMessage(Message)
	 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition_InMessage()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Message getInMessage();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getInMessage <em>In Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Message</em>' containment reference.
	 * @see #getInMessage()
	 * @generated
	 */
	void setInMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Out Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Message</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Message</em>' containment reference.
	 * @see #setOutMessage(Message)
	 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition_OutMessage()
	 * @model containment="true"
	 * @generated
	 */
	Message getOutMessage();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getOutMessage <em>Out Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Message</em>' containment reference.
	 * @see #getOutMessage()
	 * @generated
	 */
	void setOutMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Initiating Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initiating Participant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initiating Participant</em>' reference.
	 * @see #setInitiatingParticipant(Participant)
	 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition_InitiatingParticipant()
	 * @model
	 * @generated
	 */
	Participant getInitiatingParticipant();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getInitiatingParticipant <em>Initiating Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initiating Participant</em>' reference.
	 * @see #getInitiatingParticipant()
	 * @generated
	 */
	void setInitiatingParticipant(Participant value);

	/**
	 * Returns the value of the '<em><b>Receiving Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiving Participant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiving Participant</em>' reference.
	 * @see #setReceivingParticipant(Participant)
	 * @see eu.chorevolution.modelingnotations.clts.CltsPackage#getTaskTransition_ReceivingParticipant()
	 * @model required="true"
	 * @generated
	 */
	Participant getReceivingParticipant();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.clts.TaskTransition#getReceivingParticipant <em>Receiving Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiving Participant</em>' reference.
	 * @see #getReceivingParticipant()
	 * @generated
	 */
	void setReceivingParticipant(Participant value);

} // TaskTransition
