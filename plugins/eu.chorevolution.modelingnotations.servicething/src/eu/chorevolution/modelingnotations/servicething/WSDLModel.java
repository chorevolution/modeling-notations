/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WSDL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getWSDLModel()
 * @model
 * @generated
 */
public interface WSDLModel extends InterfaceDescription {
} // WSDLModel
