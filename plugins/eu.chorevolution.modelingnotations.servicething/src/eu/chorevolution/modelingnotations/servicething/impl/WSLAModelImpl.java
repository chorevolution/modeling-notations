/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;
import eu.chorevolution.modelingnotations.servicething.WSLAModel;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>WSLA Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WSLAModelImpl extends QoSDescriptionImpl implements WSLAModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WSLAModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.WSLA_MODEL;
	}

} //WSLAModelImpl
