/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.ChoreographyContext;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Choreography Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ChoreographyContextImpl extends MinimalEObjectImpl.Container implements ChoreographyContext {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoreographyContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariabilityPackage.Literals.CHOREOGRAPHY_CONTEXT;
	}

} //ChoreographyContextImpl
