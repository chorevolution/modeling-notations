/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord.impl;

import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.CoordPackage;
import eu.chorevolution.modelingnotations.coord.Notify;
import eu.chorevolution.modelingnotations.coord.Participant;
import eu.chorevolution.modelingnotations.coord.State;
import eu.chorevolution.modelingnotations.coord.Tuple;
import eu.chorevolution.modelingnotations.coord.Wait;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tuple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getSourceState <em>Source State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getTargetState <em>Target State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getAllowedOperation <em>Allowed Operation</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getAllowedComponentInTargetState <em>Allowed Component In Target State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getWait <em>Wait</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl#getNotify <em>Notify</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TupleImpl extends MinimalEObjectImpl.Container implements Tuple {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The cached value of the '{@link #getSourceState() <em>Source State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceState()
	 * @generated
	 * @ordered
	 */
	protected State sourceState;

	/**
	 * The cached value of the '{@link #getTargetState() <em>Target State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetState()
	 * @generated
	 * @ordered
	 */
	protected State targetState;

	/**
	 * The cached value of the '{@link #getAllowedOperation() <em>Allowed Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedOperation()
	 * @generated
	 * @ordered
	 */
	protected AllowedOperation allowedOperation;

	/**
	 * The cached value of the '{@link #getAllowedComponentInTargetState() <em>Allowed Component In Target State</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedComponentInTargetState()
	 * @generated
	 * @ordered
	 */
	protected EList<Participant> allowedComponentInTargetState;

	/**
	 * The default value of the '{@link #getCondition() <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected String condition = CONDITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getWait() <em>Wait</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWait()
	 * @generated
	 * @ordered
	 */
	protected Wait wait;

	/**
	 * The cached value of the '{@link #getNotify() <em>Notify</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotify()
	 * @generated
	 * @ordered
	 */
	protected Notify notify;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TupleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoordPackage.Literals.TUPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getSourceState() {
		if (sourceState != null && sourceState.eIsProxy()) {
			InternalEObject oldSourceState = (InternalEObject)sourceState;
			sourceState = (State)eResolveProxy(oldSourceState);
			if (sourceState != oldSourceState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoordPackage.TUPLE__SOURCE_STATE, oldSourceState, sourceState));
			}
		}
		return sourceState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetSourceState() {
		return sourceState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceState(State newSourceState) {
		State oldSourceState = sourceState;
		sourceState = newSourceState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__SOURCE_STATE, oldSourceState, sourceState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getTargetState() {
		if (targetState != null && targetState.eIsProxy()) {
			InternalEObject oldTargetState = (InternalEObject)targetState;
			targetState = (State)eResolveProxy(oldTargetState);
			if (targetState != oldTargetState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoordPackage.TUPLE__TARGET_STATE, oldTargetState, targetState));
			}
		}
		return targetState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetTargetState() {
		return targetState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetState(State newTargetState) {
		State oldTargetState = targetState;
		targetState = newTargetState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__TARGET_STATE, oldTargetState, targetState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllowedOperation getAllowedOperation() {
		if (allowedOperation != null && allowedOperation.eIsProxy()) {
			InternalEObject oldAllowedOperation = (InternalEObject)allowedOperation;
			allowedOperation = (AllowedOperation)eResolveProxy(oldAllowedOperation);
			if (allowedOperation != oldAllowedOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoordPackage.TUPLE__ALLOWED_OPERATION, oldAllowedOperation, allowedOperation));
			}
		}
		return allowedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllowedOperation basicGetAllowedOperation() {
		return allowedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllowedOperation(AllowedOperation newAllowedOperation) {
		AllowedOperation oldAllowedOperation = allowedOperation;
		allowedOperation = newAllowedOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__ALLOWED_OPERATION, oldAllowedOperation, allowedOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Participant> getAllowedComponentInTargetState() {
		if (allowedComponentInTargetState == null) {
			allowedComponentInTargetState = new EObjectResolvingEList<Participant>(Participant.class, this, CoordPackage.TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE);
		}
		return allowedComponentInTargetState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(String newCondition) {
		String oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__CONDITION, oldCondition, condition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Wait getWait() {
		return wait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetWait(Wait newWait, NotificationChain msgs) {
		Wait oldWait = wait;
		wait = newWait;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__WAIT, oldWait, newWait);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWait(Wait newWait) {
		if (newWait != wait) {
			NotificationChain msgs = null;
			if (wait != null)
				msgs = ((InternalEObject)wait).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoordPackage.TUPLE__WAIT, null, msgs);
			if (newWait != null)
				msgs = ((InternalEObject)newWait).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoordPackage.TUPLE__WAIT, null, msgs);
			msgs = basicSetWait(newWait, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__WAIT, newWait, newWait));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Notify getNotify() {
		return notify;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNotify(Notify newNotify, NotificationChain msgs) {
		Notify oldNotify = notify;
		notify = newNotify;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__NOTIFY, oldNotify, newNotify);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotify(Notify newNotify) {
		if (newNotify != notify) {
			NotificationChain msgs = null;
			if (notify != null)
				msgs = ((InternalEObject)notify).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoordPackage.TUPLE__NOTIFY, null, msgs);
			if (newNotify != null)
				msgs = ((InternalEObject)newNotify).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoordPackage.TUPLE__NOTIFY, null, msgs);
			msgs = basicSetNotify(newNotify, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.TUPLE__NOTIFY, newNotify, newNotify));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoordPackage.TUPLE__WAIT:
				return basicSetWait(null, msgs);
			case CoordPackage.TUPLE__NOTIFY:
				return basicSetNotify(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoordPackage.TUPLE__SOURCE_STATE:
				if (resolve) return getSourceState();
				return basicGetSourceState();
			case CoordPackage.TUPLE__TARGET_STATE:
				if (resolve) return getTargetState();
				return basicGetTargetState();
			case CoordPackage.TUPLE__ALLOWED_OPERATION:
				if (resolve) return getAllowedOperation();
				return basicGetAllowedOperation();
			case CoordPackage.TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE:
				return getAllowedComponentInTargetState();
			case CoordPackage.TUPLE__CONDITION:
				return getCondition();
			case CoordPackage.TUPLE__WAIT:
				return getWait();
			case CoordPackage.TUPLE__NOTIFY:
				return getNotify();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoordPackage.TUPLE__SOURCE_STATE:
				setSourceState((State)newValue);
				return;
			case CoordPackage.TUPLE__TARGET_STATE:
				setTargetState((State)newValue);
				return;
			case CoordPackage.TUPLE__ALLOWED_OPERATION:
				setAllowedOperation((AllowedOperation)newValue);
				return;
			case CoordPackage.TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE:
				getAllowedComponentInTargetState().clear();
				getAllowedComponentInTargetState().addAll((Collection<? extends Participant>)newValue);
				return;
			case CoordPackage.TUPLE__CONDITION:
				setCondition((String)newValue);
				return;
			case CoordPackage.TUPLE__WAIT:
				setWait((Wait)newValue);
				return;
			case CoordPackage.TUPLE__NOTIFY:
				setNotify((Notify)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoordPackage.TUPLE__SOURCE_STATE:
				setSourceState((State)null);
				return;
			case CoordPackage.TUPLE__TARGET_STATE:
				setTargetState((State)null);
				return;
			case CoordPackage.TUPLE__ALLOWED_OPERATION:
				setAllowedOperation((AllowedOperation)null);
				return;
			case CoordPackage.TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE:
				getAllowedComponentInTargetState().clear();
				return;
			case CoordPackage.TUPLE__CONDITION:
				setCondition(CONDITION_EDEFAULT);
				return;
			case CoordPackage.TUPLE__WAIT:
				setWait((Wait)null);
				return;
			case CoordPackage.TUPLE__NOTIFY:
				setNotify((Notify)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoordPackage.TUPLE__SOURCE_STATE:
				return sourceState != null;
			case CoordPackage.TUPLE__TARGET_STATE:
				return targetState != null;
			case CoordPackage.TUPLE__ALLOWED_OPERATION:
				return allowedOperation != null;
			case CoordPackage.TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE:
				return allowedComponentInTargetState != null && !allowedComponentInTargetState.isEmpty();
			case CoordPackage.TUPLE__CONDITION:
				return CONDITION_EDEFAULT == null ? condition != null : !CONDITION_EDEFAULT.equals(condition);
			case CoordPackage.TUPLE__WAIT:
				return wait != null;
			case CoordPackage.TUPLE__NOTIFY:
				return notify != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (condition: ");
		result.append(condition);
		result.append(')');
		return result.toString();
	}

} //TupleImpl
