/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;
import eu.chorevolution.modelingnotations.servicething.WSSPModel;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>WSSP Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WSSPModelImpl extends SecurityDescriptionImpl implements WSSPModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WSSPModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.WSSP_MODEL;
	}

} //WSSPModelImpl
