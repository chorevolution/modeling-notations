/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.clts.provider;


import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.CltsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link eu.chorevolution.modelingnotations.clts.Choreography} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ChoreographyItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoreographyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addChoreographyNamePropertyDescriptor(object);
			addChoreographyIDPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Choreography Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChoreographyNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Choreography_choreographyName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Choreography_choreographyName_feature", "_UI_Choreography_type"),
				 CltsPackage.Literals.CHOREOGRAPHY__CHOREOGRAPHY_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Choreography ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChoreographyIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Choreography_choreographyID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Choreography_choreographyID_feature", "_UI_Choreography_type"),
				 CltsPackage.Literals.CHOREOGRAPHY__CHOREOGRAPHY_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CltsPackage.Literals.CHOREOGRAPHY__STATES);
			childrenFeatures.add(CltsPackage.Literals.CHOREOGRAPHY__TRANSITIONS);
			childrenFeatures.add(CltsPackage.Literals.CHOREOGRAPHY__PARTICIPANTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Choreography.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Choreography"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Choreography)object).getChoreographyName();
		return label == null || label.length() == 0 ?
			getString("_UI_Choreography_type") :
			getString("_UI_Choreography_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Choreography.class)) {
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME:
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CltsPackage.CHOREOGRAPHY__STATES:
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createSimpleState()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createAlternative()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createFork()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createJoin()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createLoop()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createStartEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__STATES,
				 CltsFactory.eINSTANCE.createEndEvent()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__TRANSITIONS,
				 CltsFactory.eINSTANCE.createControlflowTransition()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__TRANSITIONS,
				 CltsFactory.eINSTANCE.createTaskTransition()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__TRANSITIONS,
				 CltsFactory.eINSTANCE.createCallTransition()));

		newChildDescriptors.add
			(createChildParameter
				(CltsPackage.Literals.CHOREOGRAPHY__PARTICIPANTS,
				 CltsFactory.eINSTANCE.createParticipant()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return CltsEditPlugin.INSTANCE;
	}

}
