/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Choreography Variant Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContext <em>Context</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getChoreographyVariants <em>Choreography Variants</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContextEvaluator <em>Context Evaluator</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreographyVariantPoint()
 * @model
 * @generated
 */
public interface CallChoreographyVariantPoint extends CallChoreography {
	/**
	 * Returns the value of the '<em><b>Context</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context</em>' attribute.
	 * @see #setContext(String)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreographyVariantPoint_Context()
	 * @model
	 * @generated
	 */
	String getContext();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContext <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context</em>' attribute.
	 * @see #getContext()
	 * @generated
	 */
	void setContext(String value);

	/**
	 * Returns the value of the '<em><b>Choreography Variants</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.variability.ChoreographyVariant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choreography Variants</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choreography Variants</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreographyVariantPoint_ChoreographyVariants()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ChoreographyVariant> getChoreographyVariants();

	/**
	 * Returns the value of the '<em><b>Context Evaluator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Context Evaluator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Context Evaluator</em>' reference.
	 * @see #setContextEvaluator(ContextEvaluator)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreographyVariantPoint_ContextEvaluator()
	 * @model required="true"
	 * @generated
	 */
	ContextEvaluator getContextEvaluator();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint#getContextEvaluator <em>Context Evaluator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Context Evaluator</em>' reference.
	 * @see #getContextEvaluator()
	 * @generated
	 */
	void setContextEvaluator(ContextEvaluator value);

} // CallChoreographyVariantPoint
