/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord.impl;

import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.CoordPackage;
import eu.chorevolution.modelingnotations.coord.Message;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Allowed Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl#getName <em>Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl#getInMessage <em>In Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl#getOutMessage <em>Out Message</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AllowedOperationImpl extends MinimalEObjectImpl.Container implements AllowedOperation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInMessage() <em>In Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInMessage()
	 * @generated
	 * @ordered
	 */
	protected Message inMessage;

	/**
	 * The cached value of the '{@link #getOutMessage() <em>Out Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutMessage()
	 * @generated
	 * @ordered
	 */
	protected Message outMessage;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AllowedOperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoordPackage.Literals.ALLOWED_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.ALLOWED_OPERATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getInMessage() {
		return inMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInMessage(Message newInMessage, NotificationChain msgs) {
		Message oldInMessage = inMessage;
		inMessage = newInMessage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoordPackage.ALLOWED_OPERATION__IN_MESSAGE, oldInMessage, newInMessage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInMessage(Message newInMessage) {
		if (newInMessage != inMessage) {
			NotificationChain msgs = null;
			if (inMessage != null)
				msgs = ((InternalEObject)inMessage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoordPackage.ALLOWED_OPERATION__IN_MESSAGE, null, msgs);
			if (newInMessage != null)
				msgs = ((InternalEObject)newInMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoordPackage.ALLOWED_OPERATION__IN_MESSAGE, null, msgs);
			msgs = basicSetInMessage(newInMessage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.ALLOWED_OPERATION__IN_MESSAGE, newInMessage, newInMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getOutMessage() {
		return outMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutMessage(Message newOutMessage, NotificationChain msgs) {
		Message oldOutMessage = outMessage;
		outMessage = newOutMessage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE, oldOutMessage, newOutMessage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutMessage(Message newOutMessage) {
		if (newOutMessage != outMessage) {
			NotificationChain msgs = null;
			if (outMessage != null)
				msgs = ((InternalEObject)outMessage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE, null, msgs);
			if (newOutMessage != null)
				msgs = ((InternalEObject)newOutMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE, null, msgs);
			msgs = basicSetOutMessage(newOutMessage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE, newOutMessage, newOutMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoordPackage.ALLOWED_OPERATION__IN_MESSAGE:
				return basicSetInMessage(null, msgs);
			case CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE:
				return basicSetOutMessage(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoordPackage.ALLOWED_OPERATION__NAME:
				return getName();
			case CoordPackage.ALLOWED_OPERATION__IN_MESSAGE:
				return getInMessage();
			case CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE:
				return getOutMessage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoordPackage.ALLOWED_OPERATION__NAME:
				setName((String)newValue);
				return;
			case CoordPackage.ALLOWED_OPERATION__IN_MESSAGE:
				setInMessage((Message)newValue);
				return;
			case CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE:
				setOutMessage((Message)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoordPackage.ALLOWED_OPERATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoordPackage.ALLOWED_OPERATION__IN_MESSAGE:
				setInMessage((Message)null);
				return;
			case CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE:
				setOutMessage((Message)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoordPackage.ALLOWED_OPERATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoordPackage.ALLOWED_OPERATION__IN_MESSAGE:
				return inMessage != null;
			case CoordPackage.ALLOWED_OPERATION__OUT_MESSAGE:
				return outMessage != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AllowedOperationImpl
