/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.SecurityDescription;
import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SecurityDescriptionImpl extends MinimalEObjectImpl.Container implements SecurityDescription {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.SECURITY_DESCRIPTION;
	}

} //SecurityDescriptionImpl
