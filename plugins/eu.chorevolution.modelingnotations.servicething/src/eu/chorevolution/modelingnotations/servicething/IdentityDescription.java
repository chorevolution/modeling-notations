/**
 */
package eu.chorevolution.modelingnotations.servicething;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Identity Description</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getIdentityDescription()
 * @model abstract="true"
 * @generated
 */
public interface IdentityDescription extends EObject {
} // IdentityDescription
