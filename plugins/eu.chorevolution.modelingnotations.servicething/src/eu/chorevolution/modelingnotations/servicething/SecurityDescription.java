/**
 */
package eu.chorevolution.modelingnotations.servicething;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security Description</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getSecurityDescription()
 * @model abstract="true"
 * @generated
 */
public interface SecurityDescription extends EObject {
} // SecurityDescription
