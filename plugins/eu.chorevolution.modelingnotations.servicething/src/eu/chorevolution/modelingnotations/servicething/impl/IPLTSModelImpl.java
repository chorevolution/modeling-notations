/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.IPLTSModel;
import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>IPLTS Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IPLTSModelImpl extends InteractionProtocolDescriptionImpl implements IPLTSModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IPLTSModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.IPLTS_MODEL;
	}

} //IPLTSModelImpl
