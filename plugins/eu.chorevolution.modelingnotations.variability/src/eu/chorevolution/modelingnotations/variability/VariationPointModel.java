/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variation Point Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.VariationPointModel#getCallChoreograpies <em>Call Choreograpies</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.VariationPointModel#getChoreography <em>Choreography</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getVariationPointModel()
 * @model
 * @generated
 */
public interface VariationPointModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Call Choreograpies</b></em>' containment reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.variability.CallChoreography}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Choreograpies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Choreograpies</em>' containment reference list.
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getVariationPointModel_CallChoreograpies()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<CallChoreography> getCallChoreograpies();

	/**
	 * Returns the value of the '<em><b>Choreography</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choreography</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choreography</em>' reference.
	 * @see #setChoreography(Choreography)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getVariationPointModel_Choreography()
	 * @model required="true"
	 * @generated
	 */
	Choreography getChoreography();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.VariationPointModel#getChoreography <em>Choreography</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choreography</em>' reference.
	 * @see #getChoreography()
	 * @generated
	 */
	void setChoreography(Choreography value);

} // VariationPointModel
