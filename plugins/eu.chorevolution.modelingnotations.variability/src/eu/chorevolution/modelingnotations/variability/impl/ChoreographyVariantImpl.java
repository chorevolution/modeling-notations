/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.ChoreographyContext;
import eu.chorevolution.modelingnotations.variability.ChoreographyVariant;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Choreography Variant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl#isIsActive <em>Is Active</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.ChoreographyVariantImpl#getChoreographyContext <em>Choreography Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChoreographyVariantImpl extends ChoreographyImpl implements ChoreographyVariant {
	/**
	 * The default value of the '{@link #isIsActive() <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ACTIVE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsActive() <em>Is Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsActive()
	 * @generated
	 * @ordered
	 */
	protected boolean isActive = IS_ACTIVE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChoreographyContext() <em>Choreography Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyContext()
	 * @generated
	 * @ordered
	 */
	protected ChoreographyContext choreographyContext;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoreographyVariantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariabilityPackage.Literals.CHOREOGRAPHY_VARIANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsActive() {
		return isActive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsActive(boolean newIsActive) {
		boolean oldIsActive = isActive;
		isActive = newIsActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariabilityPackage.CHOREOGRAPHY_VARIANT__IS_ACTIVE, oldIsActive, isActive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoreographyContext getChoreographyContext() {
		if (choreographyContext != null && choreographyContext.eIsProxy()) {
			InternalEObject oldChoreographyContext = (InternalEObject)choreographyContext;
			choreographyContext = (ChoreographyContext)eResolveProxy(oldChoreographyContext);
			if (choreographyContext != oldChoreographyContext) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT, oldChoreographyContext, choreographyContext));
			}
		}
		return choreographyContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoreographyContext basicGetChoreographyContext() {
		return choreographyContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoreographyContext(ChoreographyContext newChoreographyContext) {
		ChoreographyContext oldChoreographyContext = choreographyContext;
		choreographyContext = newChoreographyContext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT, oldChoreographyContext, choreographyContext));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__IS_ACTIVE:
				return isIsActive();
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT:
				if (resolve) return getChoreographyContext();
				return basicGetChoreographyContext();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__IS_ACTIVE:
				setIsActive((Boolean)newValue);
				return;
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT:
				setChoreographyContext((ChoreographyContext)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__IS_ACTIVE:
				setIsActive(IS_ACTIVE_EDEFAULT);
				return;
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT:
				setChoreographyContext((ChoreographyContext)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__IS_ACTIVE:
				return isActive != IS_ACTIVE_EDEFAULT;
			case VariabilityPackage.CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT:
				return choreographyContext != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (isActive: ");
		result.append(isActive);
		result.append(')');
		return result.toString();
	}

} //ChoreographyVariantImpl
