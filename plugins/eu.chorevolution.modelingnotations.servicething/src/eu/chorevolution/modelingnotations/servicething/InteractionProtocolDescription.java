/**
 */
package eu.chorevolution.modelingnotations.servicething;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Protocol Description</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getInteractionProtocolDescription()
 * @model abstract="true"
 * @generated
 */
public interface InteractionProtocolDescription extends EObject {
} // InteractionProtocolDescription
