/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Context Evaluator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getContextEvaluator()
 * @model
 * @generated
 */
public interface ContextEvaluator extends EObject {
} // ContextEvaluator
