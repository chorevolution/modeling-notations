/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.CallChoreography;
import eu.chorevolution.modelingnotations.variability.Choreography;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;
import eu.chorevolution.modelingnotations.variability.VariationPointModel;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variation Point Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl#getCallChoreograpies <em>Call Choreograpies</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.VariationPointModelImpl#getChoreography <em>Choreography</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariationPointModelImpl extends MinimalEObjectImpl.Container implements VariationPointModel {
	/**
	 * The cached value of the '{@link #getCallChoreograpies() <em>Call Choreograpies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallChoreograpies()
	 * @generated
	 * @ordered
	 */
	protected EList<CallChoreography> callChoreograpies;

	/**
	 * The cached value of the '{@link #getChoreography() <em>Choreography</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreography()
	 * @generated
	 * @ordered
	 */
	protected Choreography choreography;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariationPointModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariabilityPackage.Literals.VARIATION_POINT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CallChoreography> getCallChoreograpies() {
		if (callChoreograpies == null) {
			callChoreograpies = new EObjectContainmentEList<CallChoreography>(CallChoreography.class, this, VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES);
		}
		return callChoreograpies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choreography getChoreography() {
		if (choreography != null && choreography.eIsProxy()) {
			InternalEObject oldChoreography = (InternalEObject)choreography;
			choreography = (Choreography)eResolveProxy(oldChoreography);
			if (choreography != oldChoreography) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY, oldChoreography, choreography));
			}
		}
		return choreography;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choreography basicGetChoreography() {
		return choreography;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoreography(Choreography newChoreography) {
		Choreography oldChoreography = choreography;
		choreography = newChoreography;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY, oldChoreography, choreography));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES:
				return ((InternalEList<?>)getCallChoreograpies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES:
				return getCallChoreograpies();
			case VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY:
				if (resolve) return getChoreography();
				return basicGetChoreography();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES:
				getCallChoreograpies().clear();
				getCallChoreograpies().addAll((Collection<? extends CallChoreography>)newValue);
				return;
			case VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY:
				setChoreography((Choreography)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES:
				getCallChoreograpies().clear();
				return;
			case VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY:
				setChoreography((Choreography)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariabilityPackage.VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES:
				return callChoreograpies != null && !callChoreograpies.isEmpty();
			case VariabilityPackage.VARIATION_POINT_MODEL__CHOREOGRAPHY:
				return choreography != null;
		}
		return super.eIsSet(featureID);
	}

} //VariationPointModelImpl
