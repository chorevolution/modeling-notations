/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Choreography Context</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getChoreographyContext()
 * @model
 * @generated
 */
public interface ChoreographyContext extends EObject {
} // ChoreographyContext
