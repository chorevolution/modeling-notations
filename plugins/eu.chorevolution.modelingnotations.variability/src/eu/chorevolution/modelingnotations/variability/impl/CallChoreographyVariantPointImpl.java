/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint;
import eu.chorevolution.modelingnotations.variability.ChoreographyVariant;
import eu.chorevolution.modelingnotations.variability.ContextEvaluator;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Choreography Variant Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl#getContext <em>Context</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl#getChoreographyVariants <em>Choreography Variants</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.impl.CallChoreographyVariantPointImpl#getContextEvaluator <em>Context Evaluator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallChoreographyVariantPointImpl extends CallChoreographyImpl implements CallChoreographyVariantPoint {
	/**
	 * The default value of the '{@link #getContext() <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContext() <em>Context</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContext()
	 * @generated
	 * @ordered
	 */
	protected String context = CONTEXT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChoreographyVariants() <em>Choreography Variants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyVariants()
	 * @generated
	 * @ordered
	 */
	protected EList<ChoreographyVariant> choreographyVariants;

	/**
	 * The cached value of the '{@link #getContextEvaluator() <em>Context Evaluator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContextEvaluator()
	 * @generated
	 * @ordered
	 */
	protected ContextEvaluator contextEvaluator;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallChoreographyVariantPointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariabilityPackage.Literals.CALL_CHOREOGRAPHY_VARIANT_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContext() {
		return context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContext(String newContext) {
		String oldContext = context;
		context = newContext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT, oldContext, context));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ChoreographyVariant> getChoreographyVariants() {
		if (choreographyVariants == null) {
			choreographyVariants = new EObjectContainmentEList<ChoreographyVariant>(ChoreographyVariant.class, this, VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS);
		}
		return choreographyVariants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextEvaluator getContextEvaluator() {
		if (contextEvaluator != null && contextEvaluator.eIsProxy()) {
			InternalEObject oldContextEvaluator = (InternalEObject)contextEvaluator;
			contextEvaluator = (ContextEvaluator)eResolveProxy(oldContextEvaluator);
			if (contextEvaluator != oldContextEvaluator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR, oldContextEvaluator, contextEvaluator));
			}
		}
		return contextEvaluator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextEvaluator basicGetContextEvaluator() {
		return contextEvaluator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContextEvaluator(ContextEvaluator newContextEvaluator) {
		ContextEvaluator oldContextEvaluator = contextEvaluator;
		contextEvaluator = newContextEvaluator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR, oldContextEvaluator, contextEvaluator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS:
				return ((InternalEList<?>)getChoreographyVariants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT:
				return getContext();
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS:
				return getChoreographyVariants();
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR:
				if (resolve) return getContextEvaluator();
				return basicGetContextEvaluator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT:
				setContext((String)newValue);
				return;
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS:
				getChoreographyVariants().clear();
				getChoreographyVariants().addAll((Collection<? extends ChoreographyVariant>)newValue);
				return;
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR:
				setContextEvaluator((ContextEvaluator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT:
				setContext(CONTEXT_EDEFAULT);
				return;
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS:
				getChoreographyVariants().clear();
				return;
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR:
				setContextEvaluator((ContextEvaluator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT:
				return CONTEXT_EDEFAULT == null ? context != null : !CONTEXT_EDEFAULT.equals(context);
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS:
				return choreographyVariants != null && !choreographyVariants.isEmpty();
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR:
				return contextEvaluator != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (context: ");
		result.append(context);
		result.append(')');
		return result.toString();
	}

} //CallChoreographyVariantPointImpl
