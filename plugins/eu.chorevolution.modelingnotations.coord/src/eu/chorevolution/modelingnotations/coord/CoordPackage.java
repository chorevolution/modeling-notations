/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see eu.chorevolution.modelingnotations.coord.CoordFactory
 * @model kind="package"
 * @generated
 */
public interface CoordPackage extends EPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "coord";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://eu.chorevolution/modelingnotations/coord";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "coord";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoordPackage eINSTANCE = eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl.init();

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl <em>COORD Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getCOORDModel()
	 * @generated
	 */
	int COORD_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Choreography Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__CHOREOGRAPHY_NAME = 1;

	/**
	 * The feature id for the '<em><b>Tuples</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__TUPLES = 2;

	/**
	 * The feature id for the '<em><b>States</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__STATES = 3;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__OPERATIONS = 4;

	/**
	 * The feature id for the '<em><b>Participants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__PARTICIPANTS = 5;

	/**
	 * The feature id for the '<em><b>Initial State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL__INITIAL_STATE = 6;

	/**
	 * The number of structural features of the '<em>COORD Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>COORD Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COORD_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl <em>Tuple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.TupleImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getTuple()
	 * @generated
	 */
	int TUPLE = 1;

	/**
	 * The feature id for the '<em><b>Source State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__SOURCE_STATE = 0;

	/**
	 * The feature id for the '<em><b>Target State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__TARGET_STATE = 1;

	/**
	 * The feature id for the '<em><b>Allowed Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__ALLOWED_OPERATION = 2;

	/**
	 * The feature id for the '<em><b>Allowed Component In Target State</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE = 3;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__CONDITION = 4;

	/**
	 * The feature id for the '<em><b>Wait</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__WAIT = 5;

	/**
	 * The feature id for the '<em><b>Notify</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE__NOTIFY = 6;

	/**
	 * The number of structural features of the '<em>Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TUPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.StateImpl <em>State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.StateImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getState()
	 * @generated
	 */
	int STATE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE__NAME = 0;

	/**
	 * The number of structural features of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl <em>Allowed Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getAllowedOperation()
	 * @generated
	 */
	int ALLOWED_OPERATION = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOWED_OPERATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>In Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOWED_OPERATION__IN_MESSAGE = 1;

	/**
	 * The feature id for the '<em><b>Out Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOWED_OPERATION__OUT_MESSAGE = 2;

	/**
	 * The number of structural features of the '<em>Allowed Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOWED_OPERATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Allowed Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALLOWED_OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.MessageImpl <em>Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.MessageImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getMessage()
	 * @generated
	 */
	int MESSAGE = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__CONTENT = 1;

	/**
	 * The number of structural features of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.ParticipantImpl <em>Participant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.ParticipantImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getParticipant()
	 * @generated
	 */
	int PARTICIPANT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Participant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARTICIPANT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.NotifyImpl <em>Notify</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.NotifyImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getNotify()
	 * @generated
	 */
	int NOTIFY = 6;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY__ELEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Notify</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Notify</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.NotifyElementImpl <em>Notify Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.NotifyElementImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getNotifyElement()
	 * @generated
	 */
	int NOTIFY_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_ELEMENT__STATE = 0;

	/**
	 * The feature id for the '<em><b>Participants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_ELEMENT__PARTICIPANTS = 1;

	/**
	 * The number of structural features of the '<em>Notify Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Notify Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFY_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.WaitImpl <em>Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.WaitImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getWait()
	 * @generated
	 */
	int WAIT = 8;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT__ELEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.impl.WaitElementImpl <em>Wait Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.impl.WaitElementImpl
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getWaitElement()
	 * @generated
	 */
	int WAIT_ELEMENT = 9;

	/**
	 * The feature id for the '<em><b>State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_ELEMENT__STATE = 0;

	/**
	 * The feature id for the '<em><b>Participants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_ELEMENT__PARTICIPANTS = 1;

	/**
	 * The number of structural features of the '<em>Wait Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Wait Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link eu.chorevolution.modelingnotations.coord.MessageType <em>Message Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see eu.chorevolution.modelingnotations.coord.MessageType
	 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getMessageType()
	 * @generated
	 */
	int MESSAGE_TYPE = 10;


	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.COORDModel <em>COORD Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>COORD Model</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel
	 * @generated
	 */
	EClass getCOORDModel();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getName()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EAttribute getCOORDModel_Name();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getChoreographyName <em>Choreography Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Choreography Name</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getChoreographyName()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EAttribute getCOORDModel_ChoreographyName();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getTuples <em>Tuples</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tuples</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getTuples()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EReference getCOORDModel_Tuples();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getStates <em>States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>States</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getStates()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EReference getCOORDModel_States();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getOperations()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EReference getCOORDModel_Operations();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Participants</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getParticipants()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EReference getCOORDModel_Participants();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.COORDModel#getInitialState <em>Initial State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initial State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.COORDModel#getInitialState()
	 * @see #getCOORDModel()
	 * @generated
	 */
	EReference getCOORDModel_InitialState();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.Tuple <em>Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tuple</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple
	 * @generated
	 */
	EClass getTuple();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.Tuple#getSourceState <em>Source State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getSourceState()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_SourceState();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.Tuple#getTargetState <em>Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getTargetState()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_TargetState();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.Tuple#getAllowedOperation <em>Allowed Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Allowed Operation</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getAllowedOperation()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_AllowedOperation();

	/**
	 * Returns the meta object for the reference list '{@link eu.chorevolution.modelingnotations.coord.Tuple#getAllowedComponentInTargetState <em>Allowed Component In Target State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Allowed Component In Target State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getAllowedComponentInTargetState()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_AllowedComponentInTargetState();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.Tuple#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getCondition()
	 * @see #getTuple()
	 * @generated
	 */
	EAttribute getTuple_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link eu.chorevolution.modelingnotations.coord.Tuple#getWait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Wait</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getWait()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_Wait();

	/**
	 * Returns the meta object for the containment reference '{@link eu.chorevolution.modelingnotations.coord.Tuple#getNotify <em>Notify</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Notify</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Tuple#getNotify()
	 * @see #getTuple()
	 * @generated
	 */
	EReference getTuple_Notify();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.State
	 * @generated
	 */
	EClass getState();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.State#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.State#getName()
	 * @see #getState()
	 * @generated
	 */
	EAttribute getState_Name();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation <em>Allowed Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Allowed Operation</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.AllowedOperation
	 * @generated
	 */
	EClass getAllowedOperation();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.AllowedOperation#getName()
	 * @see #getAllowedOperation()
	 * @generated
	 */
	EAttribute getAllowedOperation_Name();

	/**
	 * Returns the meta object for the containment reference '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getInMessage <em>In Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In Message</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.AllowedOperation#getInMessage()
	 * @see #getAllowedOperation()
	 * @generated
	 */
	EReference getAllowedOperation_InMessage();

	/**
	 * Returns the meta object for the containment reference '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getOutMessage <em>Out Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Out Message</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.AllowedOperation#getOutMessage()
	 * @see #getAllowedOperation()
	 * @generated
	 */
	EReference getAllowedOperation_OutMessage();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Message
	 * @generated
	 */
	EClass getMessage();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.Message#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Message#getType()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Type();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.Message#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Message#getContent()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Content();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.Participant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Participant</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Participant
	 * @generated
	 */
	EClass getParticipant();

	/**
	 * Returns the meta object for the attribute '{@link eu.chorevolution.modelingnotations.coord.Participant#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Participant#getName()
	 * @see #getParticipant()
	 * @generated
	 */
	EAttribute getParticipant_Name();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.Notify <em>Notify</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Notify</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Notify
	 * @generated
	 */
	EClass getNotify();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.Notify#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Notify#getElements()
	 * @see #getNotify()
	 * @generated
	 */
	EReference getNotify_Elements();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.NotifyElement <em>Notify Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Notify Element</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.NotifyElement
	 * @generated
	 */
	EClass getNotifyElement();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.NotifyElement#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.NotifyElement#getState()
	 * @see #getNotifyElement()
	 * @generated
	 */
	EReference getNotifyElement_State();

	/**
	 * Returns the meta object for the reference list '{@link eu.chorevolution.modelingnotations.coord.NotifyElement#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participants</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.NotifyElement#getParticipants()
	 * @see #getNotifyElement()
	 * @generated
	 */
	EReference getNotifyElement_Participants();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.Wait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Wait
	 * @generated
	 */
	EClass getWait();

	/**
	 * Returns the meta object for the containment reference list '{@link eu.chorevolution.modelingnotations.coord.Wait#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.Wait#getElements()
	 * @see #getWait()
	 * @generated
	 */
	EReference getWait_Elements();

	/**
	 * Returns the meta object for class '{@link eu.chorevolution.modelingnotations.coord.WaitElement <em>Wait Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait Element</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.WaitElement
	 * @generated
	 */
	EClass getWaitElement();

	/**
	 * Returns the meta object for the reference '{@link eu.chorevolution.modelingnotations.coord.WaitElement#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>State</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.WaitElement#getState()
	 * @see #getWaitElement()
	 * @generated
	 */
	EReference getWaitElement_State();

	/**
	 * Returns the meta object for the reference list '{@link eu.chorevolution.modelingnotations.coord.WaitElement#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participants</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.WaitElement#getParticipants()
	 * @see #getWaitElement()
	 * @generated
	 */
	EReference getWaitElement_Participants();

	/**
	 * Returns the meta object for enum '{@link eu.chorevolution.modelingnotations.coord.MessageType <em>Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Message Type</em>'.
	 * @see eu.chorevolution.modelingnotations.coord.MessageType
	 * @generated
	 */
	EEnum getMessageType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoordFactory getCoordFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl <em>COORD Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getCOORDModel()
		 * @generated
		 */
		EClass COORD_MODEL = eINSTANCE.getCOORDModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORD_MODEL__NAME = eINSTANCE.getCOORDModel_Name();

		/**
		 * The meta object literal for the '<em><b>Choreography Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COORD_MODEL__CHOREOGRAPHY_NAME = eINSTANCE.getCOORDModel_ChoreographyName();

		/**
		 * The meta object literal for the '<em><b>Tuples</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORD_MODEL__TUPLES = eINSTANCE.getCOORDModel_Tuples();

		/**
		 * The meta object literal for the '<em><b>States</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORD_MODEL__STATES = eINSTANCE.getCOORDModel_States();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORD_MODEL__OPERATIONS = eINSTANCE.getCOORDModel_Operations();

		/**
		 * The meta object literal for the '<em><b>Participants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORD_MODEL__PARTICIPANTS = eINSTANCE.getCOORDModel_Participants();

		/**
		 * The meta object literal for the '<em><b>Initial State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COORD_MODEL__INITIAL_STATE = eINSTANCE.getCOORDModel_InitialState();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.TupleImpl <em>Tuple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.TupleImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getTuple()
		 * @generated
		 */
		EClass TUPLE = eINSTANCE.getTuple();

		/**
		 * The meta object literal for the '<em><b>Source State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__SOURCE_STATE = eINSTANCE.getTuple_SourceState();

		/**
		 * The meta object literal for the '<em><b>Target State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__TARGET_STATE = eINSTANCE.getTuple_TargetState();

		/**
		 * The meta object literal for the '<em><b>Allowed Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__ALLOWED_OPERATION = eINSTANCE.getTuple_AllowedOperation();

		/**
		 * The meta object literal for the '<em><b>Allowed Component In Target State</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__ALLOWED_COMPONENT_IN_TARGET_STATE = eINSTANCE.getTuple_AllowedComponentInTargetState();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TUPLE__CONDITION = eINSTANCE.getTuple_Condition();

		/**
		 * The meta object literal for the '<em><b>Wait</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__WAIT = eINSTANCE.getTuple_Wait();

		/**
		 * The meta object literal for the '<em><b>Notify</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TUPLE__NOTIFY = eINSTANCE.getTuple_Notify();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.StateImpl <em>State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.StateImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getState()
		 * @generated
		 */
		EClass STATE = eINSTANCE.getState();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STATE__NAME = eINSTANCE.getState_Name();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl <em>Allowed Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.AllowedOperationImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getAllowedOperation()
		 * @generated
		 */
		EClass ALLOWED_OPERATION = eINSTANCE.getAllowedOperation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALLOWED_OPERATION__NAME = eINSTANCE.getAllowedOperation_Name();

		/**
		 * The meta object literal for the '<em><b>In Message</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOWED_OPERATION__IN_MESSAGE = eINSTANCE.getAllowedOperation_InMessage();

		/**
		 * The meta object literal for the '<em><b>Out Message</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALLOWED_OPERATION__OUT_MESSAGE = eINSTANCE.getAllowedOperation_OutMessage();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.MessageImpl <em>Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.MessageImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getMessage()
		 * @generated
		 */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__TYPE = eINSTANCE.getMessage_Type();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__CONTENT = eINSTANCE.getMessage_Content();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.ParticipantImpl <em>Participant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.ParticipantImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getParticipant()
		 * @generated
		 */
		EClass PARTICIPANT = eINSTANCE.getParticipant();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PARTICIPANT__NAME = eINSTANCE.getParticipant_Name();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.NotifyImpl <em>Notify</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.NotifyImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getNotify()
		 * @generated
		 */
		EClass NOTIFY = eINSTANCE.getNotify();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOTIFY__ELEMENTS = eINSTANCE.getNotify_Elements();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.NotifyElementImpl <em>Notify Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.NotifyElementImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getNotifyElement()
		 * @generated
		 */
		EClass NOTIFY_ELEMENT = eINSTANCE.getNotifyElement();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOTIFY_ELEMENT__STATE = eINSTANCE.getNotifyElement_State();

		/**
		 * The meta object literal for the '<em><b>Participants</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOTIFY_ELEMENT__PARTICIPANTS = eINSTANCE.getNotifyElement_Participants();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.WaitImpl <em>Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.WaitImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getWait()
		 * @generated
		 */
		EClass WAIT = eINSTANCE.getWait();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT__ELEMENTS = eINSTANCE.getWait_Elements();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.impl.WaitElementImpl <em>Wait Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.impl.WaitElementImpl
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getWaitElement()
		 * @generated
		 */
		EClass WAIT_ELEMENT = eINSTANCE.getWaitElement();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT_ELEMENT__STATE = eINSTANCE.getWaitElement_State();

		/**
		 * The meta object literal for the '<em><b>Participants</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT_ELEMENT__PARTICIPANTS = eINSTANCE.getWaitElement_Participants();

		/**
		 * The meta object literal for the '{@link eu.chorevolution.modelingnotations.coord.MessageType <em>Message Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see eu.chorevolution.modelingnotations.coord.MessageType
		 * @see eu.chorevolution.modelingnotations.coord.impl.CoordPackageImpl#getMessageType()
		 * @generated
		 */
		EEnum MESSAGE_TYPE = eINSTANCE.getMessageType();

	}

} //CoordPackage
