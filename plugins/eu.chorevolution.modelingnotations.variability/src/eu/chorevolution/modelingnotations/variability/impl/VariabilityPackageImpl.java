/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.CallChoreography;
import eu.chorevolution.modelingnotations.variability.CallChoreographyVariantPoint;
import eu.chorevolution.modelingnotations.variability.Choreography;
import eu.chorevolution.modelingnotations.variability.ChoreographyContext;
import eu.chorevolution.modelingnotations.variability.ChoreographyVariant;
import eu.chorevolution.modelingnotations.variability.ContextEvaluator;
import eu.chorevolution.modelingnotations.variability.VariabilityFactory;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;
import eu.chorevolution.modelingnotations.variability.VariationPointModel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VariabilityPackageImpl extends EPackageImpl implements VariabilityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variationPointModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choreographyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callChoreographyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callChoreographyVariantPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choreographyVariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass choreographyContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contextEvaluatorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VariabilityPackageImpl() {
		super(eNS_URI, VariabilityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VariabilityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VariabilityPackage init() {
		if (isInited) return (VariabilityPackage)EPackage.Registry.INSTANCE.getEPackage(VariabilityPackage.eNS_URI);

		// Obtain or create and register package
		VariabilityPackageImpl theVariabilityPackage = (VariabilityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VariabilityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VariabilityPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theVariabilityPackage.createPackageContents();

		// Initialize created meta-data
		theVariabilityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVariabilityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VariabilityPackage.eNS_URI, theVariabilityPackage);
		return theVariabilityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariationPointModel() {
		return variationPointModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariationPointModel_CallChoreograpies() {
		return (EReference)variationPointModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariationPointModel_Choreography() {
		return (EReference)variationPointModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoreography() {
		return choreographyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChoreography_Name() {
		return (EAttribute)choreographyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallChoreography() {
		return callChoreographyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCallChoreography_Name() {
		return (EAttribute)callChoreographyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallChoreography_CalledChoreograpies() {
		return (EReference)callChoreographyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallChoreographyVariantPoint() {
		return callChoreographyVariantPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCallChoreographyVariantPoint_Context() {
		return (EAttribute)callChoreographyVariantPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallChoreographyVariantPoint_ChoreographyVariants() {
		return (EReference)callChoreographyVariantPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallChoreographyVariantPoint_ContextEvaluator() {
		return (EReference)callChoreographyVariantPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoreographyVariant() {
		return choreographyVariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChoreographyVariant_IsActive() {
		return (EAttribute)choreographyVariantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChoreographyVariant_ChoreographyContext() {
		return (EReference)choreographyVariantEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChoreographyContext() {
		return choreographyContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContextEvaluator() {
		return contextEvaluatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariabilityFactory getVariabilityFactory() {
		return (VariabilityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		variationPointModelEClass = createEClass(VARIATION_POINT_MODEL);
		createEReference(variationPointModelEClass, VARIATION_POINT_MODEL__CALL_CHOREOGRAPIES);
		createEReference(variationPointModelEClass, VARIATION_POINT_MODEL__CHOREOGRAPHY);

		choreographyEClass = createEClass(CHOREOGRAPHY);
		createEAttribute(choreographyEClass, CHOREOGRAPHY__NAME);

		callChoreographyEClass = createEClass(CALL_CHOREOGRAPHY);
		createEAttribute(callChoreographyEClass, CALL_CHOREOGRAPHY__NAME);
		createEReference(callChoreographyEClass, CALL_CHOREOGRAPHY__CALLED_CHOREOGRAPIES);

		callChoreographyVariantPointEClass = createEClass(CALL_CHOREOGRAPHY_VARIANT_POINT);
		createEAttribute(callChoreographyVariantPointEClass, CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT);
		createEReference(callChoreographyVariantPointEClass, CALL_CHOREOGRAPHY_VARIANT_POINT__CHOREOGRAPHY_VARIANTS);
		createEReference(callChoreographyVariantPointEClass, CALL_CHOREOGRAPHY_VARIANT_POINT__CONTEXT_EVALUATOR);

		choreographyVariantEClass = createEClass(CHOREOGRAPHY_VARIANT);
		createEAttribute(choreographyVariantEClass, CHOREOGRAPHY_VARIANT__IS_ACTIVE);
		createEReference(choreographyVariantEClass, CHOREOGRAPHY_VARIANT__CHOREOGRAPHY_CONTEXT);

		choreographyContextEClass = createEClass(CHOREOGRAPHY_CONTEXT);

		contextEvaluatorEClass = createEClass(CONTEXT_EVALUATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		callChoreographyVariantPointEClass.getESuperTypes().add(this.getCallChoreography());
		choreographyVariantEClass.getESuperTypes().add(this.getChoreography());

		// Initialize classes, features, and operations; add parameters
		initEClass(variationPointModelEClass, VariationPointModel.class, "VariationPointModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariationPointModel_CallChoreograpies(), this.getCallChoreography(), null, "callChoreograpies", null, 1, -1, VariationPointModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariationPointModel_Choreography(), this.getChoreography(), null, "choreography", null, 1, 1, VariationPointModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(choreographyEClass, Choreography.class, "Choreography", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChoreography_Name(), ecorePackage.getEString(), "name", null, 1, 1, Choreography.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callChoreographyEClass, CallChoreography.class, "CallChoreography", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCallChoreography_Name(), ecorePackage.getEString(), "name", null, 1, 1, CallChoreography.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCallChoreography_CalledChoreograpies(), this.getChoreography(), null, "calledChoreograpies", null, 0, -1, CallChoreography.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callChoreographyVariantPointEClass, CallChoreographyVariantPoint.class, "CallChoreographyVariantPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCallChoreographyVariantPoint_Context(), ecorePackage.getEString(), "context", null, 0, 1, CallChoreographyVariantPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCallChoreographyVariantPoint_ChoreographyVariants(), this.getChoreographyVariant(), null, "choreographyVariants", null, 1, -1, CallChoreographyVariantPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCallChoreographyVariantPoint_ContextEvaluator(), this.getContextEvaluator(), null, "contextEvaluator", null, 1, 1, CallChoreographyVariantPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(choreographyVariantEClass, ChoreographyVariant.class, "ChoreographyVariant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChoreographyVariant_IsActive(), ecorePackage.getEBoolean(), "isActive", "false", 1, 1, ChoreographyVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getChoreographyVariant_ChoreographyContext(), this.getChoreographyContext(), null, "choreographyContext", null, 1, 1, ChoreographyVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(choreographyContextEClass, ChoreographyContext.class, "ChoreographyContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contextEvaluatorEClass, ContextEvaluator.class, "ContextEvaluator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //VariabilityPackageImpl
