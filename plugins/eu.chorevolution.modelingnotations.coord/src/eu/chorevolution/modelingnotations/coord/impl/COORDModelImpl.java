/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord.impl;

import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.modelingnotations.coord.CoordPackage;
import eu.chorevolution.modelingnotations.coord.Participant;
import eu.chorevolution.modelingnotations.coord.State;
import eu.chorevolution.modelingnotations.coord.Tuple;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COORD Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getChoreographyName <em>Choreography Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getTuples <em>Tuples</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getStates <em>States</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getOperations <em>Operations</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getParticipants <em>Participants</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.impl.COORDModelImpl#getInitialState <em>Initial State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class COORDModelImpl extends MinimalEObjectImpl.Container implements COORDModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getChoreographyName() <em>Choreography Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyName()
	 * @generated
	 * @ordered
	 */
	protected static final String CHOREOGRAPHY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChoreographyName() <em>Choreography Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyName()
	 * @generated
	 * @ordered
	 */
	protected String choreographyName = CHOREOGRAPHY_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTuples() <em>Tuples</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTuples()
	 * @generated
	 * @ordered
	 */
	protected EList<Tuple> tuples;

	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<AllowedOperation> operations;

	/**
	 * The cached value of the '{@link #getParticipants() <em>Participants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipants()
	 * @generated
	 * @ordered
	 */
	protected EList<Participant> participants;

	/**
	 * The cached value of the '{@link #getInitialState() <em>Initial State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialState()
	 * @generated
	 * @ordered
	 */
	protected State initialState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COORDModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoordPackage.Literals.COORD_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.COORD_MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChoreographyName() {
		return choreographyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoreographyName(String newChoreographyName) {
		String oldChoreographyName = choreographyName;
		choreographyName = newChoreographyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.COORD_MODEL__CHOREOGRAPHY_NAME, oldChoreographyName, choreographyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Tuple> getTuples() {
		if (tuples == null) {
			tuples = new EObjectContainmentEList<Tuple>(Tuple.class, this, CoordPackage.COORD_MODEL__TUPLES);
		}
		return tuples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, CoordPackage.COORD_MODEL__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AllowedOperation> getOperations() {
		if (operations == null) {
			operations = new EObjectContainmentEList<AllowedOperation>(AllowedOperation.class, this, CoordPackage.COORD_MODEL__OPERATIONS);
		}
		return operations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Participant> getParticipants() {
		if (participants == null) {
			participants = new EObjectContainmentEList<Participant>(Participant.class, this, CoordPackage.COORD_MODEL__PARTICIPANTS);
		}
		return participants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getInitialState() {
		if (initialState != null && initialState.eIsProxy()) {
			InternalEObject oldInitialState = (InternalEObject)initialState;
			initialState = (State)eResolveProxy(oldInitialState);
			if (initialState != oldInitialState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoordPackage.COORD_MODEL__INITIAL_STATE, oldInitialState, initialState));
			}
		}
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetInitialState() {
		return initialState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialState(State newInitialState) {
		State oldInitialState = initialState;
		initialState = newInitialState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoordPackage.COORD_MODEL__INITIAL_STATE, oldInitialState, initialState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoordPackage.COORD_MODEL__TUPLES:
				return ((InternalEList<?>)getTuples()).basicRemove(otherEnd, msgs);
			case CoordPackage.COORD_MODEL__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case CoordPackage.COORD_MODEL__OPERATIONS:
				return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
			case CoordPackage.COORD_MODEL__PARTICIPANTS:
				return ((InternalEList<?>)getParticipants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoordPackage.COORD_MODEL__NAME:
				return getName();
			case CoordPackage.COORD_MODEL__CHOREOGRAPHY_NAME:
				return getChoreographyName();
			case CoordPackage.COORD_MODEL__TUPLES:
				return getTuples();
			case CoordPackage.COORD_MODEL__STATES:
				return getStates();
			case CoordPackage.COORD_MODEL__OPERATIONS:
				return getOperations();
			case CoordPackage.COORD_MODEL__PARTICIPANTS:
				return getParticipants();
			case CoordPackage.COORD_MODEL__INITIAL_STATE:
				if (resolve) return getInitialState();
				return basicGetInitialState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoordPackage.COORD_MODEL__NAME:
				setName((String)newValue);
				return;
			case CoordPackage.COORD_MODEL__CHOREOGRAPHY_NAME:
				setChoreographyName((String)newValue);
				return;
			case CoordPackage.COORD_MODEL__TUPLES:
				getTuples().clear();
				getTuples().addAll((Collection<? extends Tuple>)newValue);
				return;
			case CoordPackage.COORD_MODEL__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case CoordPackage.COORD_MODEL__OPERATIONS:
				getOperations().clear();
				getOperations().addAll((Collection<? extends AllowedOperation>)newValue);
				return;
			case CoordPackage.COORD_MODEL__PARTICIPANTS:
				getParticipants().clear();
				getParticipants().addAll((Collection<? extends Participant>)newValue);
				return;
			case CoordPackage.COORD_MODEL__INITIAL_STATE:
				setInitialState((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoordPackage.COORD_MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoordPackage.COORD_MODEL__CHOREOGRAPHY_NAME:
				setChoreographyName(CHOREOGRAPHY_NAME_EDEFAULT);
				return;
			case CoordPackage.COORD_MODEL__TUPLES:
				getTuples().clear();
				return;
			case CoordPackage.COORD_MODEL__STATES:
				getStates().clear();
				return;
			case CoordPackage.COORD_MODEL__OPERATIONS:
				getOperations().clear();
				return;
			case CoordPackage.COORD_MODEL__PARTICIPANTS:
				getParticipants().clear();
				return;
			case CoordPackage.COORD_MODEL__INITIAL_STATE:
				setInitialState((State)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoordPackage.COORD_MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoordPackage.COORD_MODEL__CHOREOGRAPHY_NAME:
				return CHOREOGRAPHY_NAME_EDEFAULT == null ? choreographyName != null : !CHOREOGRAPHY_NAME_EDEFAULT.equals(choreographyName);
			case CoordPackage.COORD_MODEL__TUPLES:
				return tuples != null && !tuples.isEmpty();
			case CoordPackage.COORD_MODEL__STATES:
				return states != null && !states.isEmpty();
			case CoordPackage.COORD_MODEL__OPERATIONS:
				return operations != null && !operations.isEmpty();
			case CoordPackage.COORD_MODEL__PARTICIPANTS:
				return participants != null && !participants.isEmpty();
			case CoordPackage.COORD_MODEL__INITIAL_STATE:
				return initialState != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", choreographyName: ");
		result.append(choreographyName);
		result.append(')');
		return result.toString();
	}

} //COORDModelImpl
