/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.ContextEvaluator;
import eu.chorevolution.modelingnotations.variability.VariabilityPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Context Evaluator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ContextEvaluatorImpl extends MinimalEObjectImpl.Container implements ContextEvaluator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContextEvaluatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VariabilityPackage.Literals.CONTEXT_EVALUATOR;
	}

} //ContextEvaluatorImpl
