/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.clts.impl;

import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.CltsPackage;
import eu.chorevolution.modelingnotations.clts.Participant;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.modelingnotations.clts.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Choreography</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.ChoreographyImpl#getChoreographyName <em>Choreography Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.ChoreographyImpl#getChoreographyID <em>Choreography ID</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.ChoreographyImpl#getStates <em>States</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.ChoreographyImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.ChoreographyImpl#getParticipants <em>Participants</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChoreographyImpl extends MinimalEObjectImpl.Container implements Choreography {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The default value of the '{@link #getChoreographyName() <em>Choreography Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyName()
	 * @generated
	 * @ordered
	 */
	protected static final String CHOREOGRAPHY_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChoreographyName() <em>Choreography Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyName()
	 * @generated
	 * @ordered
	 */
	protected String choreographyName = CHOREOGRAPHY_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getChoreographyID() <em>Choreography ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyID()
	 * @generated
	 * @ordered
	 */
	protected static final String CHOREOGRAPHY_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getChoreographyID() <em>Choreography ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoreographyID()
	 * @generated
	 * @ordered
	 */
	protected String choreographyID = CHOREOGRAPHY_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The cached value of the '{@link #getParticipants() <em>Participants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipants()
	 * @generated
	 * @ordered
	 */
	protected EList<Participant> participants;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChoreographyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CltsPackage.Literals.CHOREOGRAPHY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChoreographyName() {
		return choreographyName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoreographyName(String newChoreographyName) {
		String oldChoreographyName = choreographyName;
		choreographyName = newChoreographyName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME, oldChoreographyName, choreographyName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChoreographyID() {
		return choreographyID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoreographyID(String newChoreographyID) {
		String oldChoreographyID = choreographyID;
		choreographyID = newChoreographyID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID, oldChoreographyID, choreographyID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, CltsPackage.CHOREOGRAPHY__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, CltsPackage.CHOREOGRAPHY__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Participant> getParticipants() {
		if (participants == null) {
			participants = new EObjectContainmentEList<Participant>(Participant.class, this, CltsPackage.CHOREOGRAPHY__PARTICIPANTS);
		}
		return participants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CltsPackage.CHOREOGRAPHY__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				return ((InternalEList<?>)getParticipants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME:
				return getChoreographyName();
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID:
				return getChoreographyID();
			case CltsPackage.CHOREOGRAPHY__STATES:
				return getStates();
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
				return getTransitions();
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				return getParticipants();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME:
				setChoreographyName((String)newValue);
				return;
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID:
				setChoreographyID((String)newValue);
				return;
			case CltsPackage.CHOREOGRAPHY__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				getParticipants().clear();
				getParticipants().addAll((Collection<? extends Participant>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME:
				setChoreographyName(CHOREOGRAPHY_NAME_EDEFAULT);
				return;
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID:
				setChoreographyID(CHOREOGRAPHY_ID_EDEFAULT);
				return;
			case CltsPackage.CHOREOGRAPHY__STATES:
				getStates().clear();
				return;
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
				getTransitions().clear();
				return;
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				getParticipants().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_NAME:
				return CHOREOGRAPHY_NAME_EDEFAULT == null ? choreographyName != null : !CHOREOGRAPHY_NAME_EDEFAULT.equals(choreographyName);
			case CltsPackage.CHOREOGRAPHY__CHOREOGRAPHY_ID:
				return CHOREOGRAPHY_ID_EDEFAULT == null ? choreographyID != null : !CHOREOGRAPHY_ID_EDEFAULT.equals(choreographyID);
			case CltsPackage.CHOREOGRAPHY__STATES:
				return states != null && !states.isEmpty();
			case CltsPackage.CHOREOGRAPHY__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case CltsPackage.CHOREOGRAPHY__PARTICIPANTS:
				return participants != null && !participants.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (choreographyName: ");
		result.append(choreographyName);
		result.append(", choreographyID: ");
		result.append(choreographyID);
		result.append(')');
		return result.toString();
	}

} //ChoreographyImpl
