/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getSourceState <em>Source State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getTargetState <em>Target State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getAllowedOperation <em>Allowed Operation</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getAllowedComponentInTargetState <em>Allowed Component In Target State</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getCondition <em>Condition</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getWait <em>Wait</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.Tuple#getNotify <em>Notify</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple()
 * @model
 * @generated
 */
public interface Tuple extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * Returns the value of the '<em><b>Source State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source State</em>' reference.
	 * @see #setSourceState(State)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_SourceState()
	 * @model required="true"
	 * @generated
	 */
	State getSourceState();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getSourceState <em>Source State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source State</em>' reference.
	 * @see #getSourceState()
	 * @generated
	 */
	void setSourceState(State value);

	/**
	 * Returns the value of the '<em><b>Target State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target State</em>' reference.
	 * @see #setTargetState(State)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_TargetState()
	 * @model required="true"
	 * @generated
	 */
	State getTargetState();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getTargetState <em>Target State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target State</em>' reference.
	 * @see #getTargetState()
	 * @generated
	 */
	void setTargetState(State value);

	/**
	 * Returns the value of the '<em><b>Allowed Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allowed Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allowed Operation</em>' reference.
	 * @see #setAllowedOperation(AllowedOperation)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_AllowedOperation()
	 * @model
	 * @generated
	 */
	AllowedOperation getAllowedOperation();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getAllowedOperation <em>Allowed Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Allowed Operation</em>' reference.
	 * @see #getAllowedOperation()
	 * @generated
	 */
	void setAllowedOperation(AllowedOperation value);

	/**
	 * Returns the value of the '<em><b>Allowed Component In Target State</b></em>' reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.coord.Participant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Allowed Component In Target State</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Allowed Component In Target State</em>' reference list.
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_AllowedComponentInTargetState()
	 * @model
	 * @generated
	 */
	EList<Participant> getAllowedComponentInTargetState();

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see #setCondition(String)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_Condition()
	 * @model
	 * @generated
	 */
	String getCondition();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' attribute.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(String value);

	/**
	 * Returns the value of the '<em><b>Wait</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wait</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wait</em>' containment reference.
	 * @see #setWait(Wait)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_Wait()
	 * @model containment="true"
	 * @generated
	 */
	Wait getWait();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getWait <em>Wait</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wait</em>' containment reference.
	 * @see #getWait()
	 * @generated
	 */
	void setWait(Wait value);

	/**
	 * Returns the value of the '<em><b>Notify</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notify</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notify</em>' containment reference.
	 * @see #setNotify(Notify)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getTuple_Notify()
	 * @model containment="true"
	 * @generated
	 */
	Notify getNotify();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.Tuple#getNotify <em>Notify</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Notify</em>' containment reference.
	 * @see #getNotify()
	 * @generated
	 */
	void setNotify(Notify value);

} // Tuple
