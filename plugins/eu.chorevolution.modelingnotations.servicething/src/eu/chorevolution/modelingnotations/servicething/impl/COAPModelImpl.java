/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.COAPModel;
import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>COAP Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class COAPModelImpl extends InterfaceDescriptionImpl implements COAPModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected COAPModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.COAP_MODEL;
	}

} //COAPModelImpl
