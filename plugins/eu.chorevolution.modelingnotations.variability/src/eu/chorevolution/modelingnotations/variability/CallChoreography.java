/**
 */
package eu.chorevolution.modelingnotations.variability;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Choreography</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.CallChoreography#getName <em>Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.variability.CallChoreography#getCalledChoreograpies <em>Called Choreograpies</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreography()
 * @model
 * @generated
 */
public interface CallChoreography extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreography_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.variability.CallChoreography#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Called Choreograpies</b></em>' reference list.
	 * The list contents are of type {@link eu.chorevolution.modelingnotations.variability.Choreography}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Called Choreograpies</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Called Choreograpies</em>' reference list.
	 * @see eu.chorevolution.modelingnotations.variability.VariabilityPackage#getCallChoreography_CalledChoreograpies()
	 * @model
	 * @generated
	 */
	EList<Choreography> getCalledChoreograpies();

} // CallChoreography
