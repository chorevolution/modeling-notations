/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WSLA Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getWSLAModel()
 * @model
 * @generated
 */
public interface WSLAModel extends QoSDescription {
} // WSLAModel
