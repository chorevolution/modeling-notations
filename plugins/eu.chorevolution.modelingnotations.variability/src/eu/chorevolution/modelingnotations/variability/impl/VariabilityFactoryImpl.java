/**
 */
package eu.chorevolution.modelingnotations.variability.impl;

import eu.chorevolution.modelingnotations.variability.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VariabilityFactoryImpl extends EFactoryImpl implements VariabilityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VariabilityFactory init() {
		try {
			VariabilityFactory theVariabilityFactory = (VariabilityFactory)EPackage.Registry.INSTANCE.getEFactory(VariabilityPackage.eNS_URI);
			if (theVariabilityFactory != null) {
				return theVariabilityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VariabilityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariabilityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VariabilityPackage.VARIATION_POINT_MODEL: return createVariationPointModel();
			case VariabilityPackage.CHOREOGRAPHY: return createChoreography();
			case VariabilityPackage.CALL_CHOREOGRAPHY: return createCallChoreography();
			case VariabilityPackage.CALL_CHOREOGRAPHY_VARIANT_POINT: return createCallChoreographyVariantPoint();
			case VariabilityPackage.CHOREOGRAPHY_VARIANT: return createChoreographyVariant();
			case VariabilityPackage.CHOREOGRAPHY_CONTEXT: return createChoreographyContext();
			case VariabilityPackage.CONTEXT_EVALUATOR: return createContextEvaluator();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariationPointModel createVariationPointModel() {
		VariationPointModelImpl variationPointModel = new VariationPointModelImpl();
		return variationPointModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Choreography createChoreography() {
		ChoreographyImpl choreography = new ChoreographyImpl();
		return choreography;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallChoreography createCallChoreography() {
		CallChoreographyImpl callChoreography = new CallChoreographyImpl();
		return callChoreography;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallChoreographyVariantPoint createCallChoreographyVariantPoint() {
		CallChoreographyVariantPointImpl callChoreographyVariantPoint = new CallChoreographyVariantPointImpl();
		return callChoreographyVariantPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoreographyVariant createChoreographyVariant() {
		ChoreographyVariantImpl choreographyVariant = new ChoreographyVariantImpl();
		return choreographyVariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChoreographyContext createChoreographyContext() {
		ChoreographyContextImpl choreographyContext = new ChoreographyContextImpl();
		return choreographyContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContextEvaluator createContextEvaluator() {
		ContextEvaluatorImpl contextEvaluator = new ContextEvaluatorImpl();
		return contextEvaluator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariabilityPackage getVariabilityPackage() {
		return (VariabilityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VariabilityPackage getPackage() {
		return VariabilityPackage.eINSTANCE;
	}

} //VariabilityFactoryImpl
