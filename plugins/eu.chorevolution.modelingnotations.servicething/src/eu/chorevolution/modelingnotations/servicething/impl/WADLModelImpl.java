/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;
import eu.chorevolution.modelingnotations.servicething.WADLModel;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>WADL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WADLModelImpl extends InterfaceDescriptionImpl implements WADLModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WADLModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.WADL_MODEL;
	}

} //WADLModelImpl
