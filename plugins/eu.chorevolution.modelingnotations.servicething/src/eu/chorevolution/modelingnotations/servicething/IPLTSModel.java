/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IPLTS Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getIPLTSModel()
 * @model
 * @generated
 */
public interface IPLTSModel extends InteractionProtocolDescription {
} // IPLTSModel
