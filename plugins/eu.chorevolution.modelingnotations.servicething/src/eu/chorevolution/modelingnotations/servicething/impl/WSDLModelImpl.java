/**
 */
package eu.chorevolution.modelingnotations.servicething.impl;

import eu.chorevolution.modelingnotations.servicething.ServicethingPackage;
import eu.chorevolution.modelingnotations.servicething.WSDLModel;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>WSDL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WSDLModelImpl extends InterfaceDescriptionImpl implements WSDLModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WSDLModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ServicethingPackage.Literals.WSDL_MODEL;
	}

} //WSDLModelImpl
