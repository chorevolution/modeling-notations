/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.coord;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Allowed Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getName <em>Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getInMessage <em>In Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getOutMessage <em>Out Message</em>}</li>
 * </ul>
 *
 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getAllowedOperation()
 * @model
 * @generated
 */
public interface AllowedOperation extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getAllowedOperation_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>In Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>In Message</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>In Message</em>' containment reference.
	 * @see #setInMessage(Message)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getAllowedOperation_InMessage()
	 * @model containment="true"
	 * @generated
	 */
	Message getInMessage();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getInMessage <em>In Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>In Message</em>' containment reference.
	 * @see #getInMessage()
	 * @generated
	 */
	void setInMessage(Message value);

	/**
	 * Returns the value of the '<em><b>Out Message</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Out Message</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Out Message</em>' containment reference.
	 * @see #setOutMessage(Message)
	 * @see eu.chorevolution.modelingnotations.coord.CoordPackage#getAllowedOperation_OutMessage()
	 * @model containment="true"
	 * @generated
	 */
	Message getOutMessage();

	/**
	 * Sets the value of the '{@link eu.chorevolution.modelingnotations.coord.AllowedOperation#getOutMessage <em>Out Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Out Message</em>' containment reference.
	 * @see #getOutMessage()
	 * @generated
	 */
	void setOutMessage(Message value);

} // AllowedOperation
