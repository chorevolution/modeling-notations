/**
 */
package eu.chorevolution.modelingnotations.servicething;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>WSBPEL Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see eu.chorevolution.modelingnotations.servicething.ServicethingPackage#getWSBPELModel()
 * @model
 * @generated
 */
public interface WSBPELModel extends InteractionProtocolDescription {
} // WSBPELModel
