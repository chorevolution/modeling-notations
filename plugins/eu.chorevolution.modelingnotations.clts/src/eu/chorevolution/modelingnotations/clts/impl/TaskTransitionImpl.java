/**
 * Copyright 2015 The CHOReVOLUTION project
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *       http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.modelingnotations.clts.impl;

import eu.chorevolution.modelingnotations.clts.CltsPackage;
import eu.chorevolution.modelingnotations.clts.Message;
import eu.chorevolution.modelingnotations.clts.Participant;
import eu.chorevolution.modelingnotations.clts.TaskTransition;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.TaskTransitionImpl#getTaskName <em>Task Name</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.TaskTransitionImpl#getInMessage <em>In Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.TaskTransitionImpl#getOutMessage <em>Out Message</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.TaskTransitionImpl#getInitiatingParticipant <em>Initiating Participant</em>}</li>
 *   <li>{@link eu.chorevolution.modelingnotations.clts.impl.TaskTransitionImpl#getReceivingParticipant <em>Receiving Participant</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskTransitionImpl extends TransitionImpl implements TaskTransition {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "Copyright 2015 The CHOReVOLUTION project\r\n\r\nLicensed under the Apache License, Version 2.0 (the \"License\");\r\nyou may not use this file except in compliance with the License.\r\nYou may obtain a copy of the License at\r\n\r\n      http://www.apache.org/licenses/LICENSE-2.0\r\n\r\nUnless required by applicable law or agreed to in writing, software\r\ndistributed under the License is distributed on an \"AS IS\" BASIS,\r\nWITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\r\nSee the License for the specific language governing permissions and\r\nlimitations under the License.";

	/**
	 * The default value of the '{@link #getTaskName() <em>Task Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskName()
	 * @generated
	 * @ordered
	 */
	protected static final String TASK_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTaskName() <em>Task Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskName()
	 * @generated
	 * @ordered
	 */
	protected String taskName = TASK_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInMessage() <em>In Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInMessage()
	 * @generated
	 * @ordered
	 */
	protected Message inMessage;

	/**
	 * The cached value of the '{@link #getOutMessage() <em>Out Message</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutMessage()
	 * @generated
	 * @ordered
	 */
	protected Message outMessage;

	/**
	 * The cached value of the '{@link #getInitiatingParticipant() <em>Initiating Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitiatingParticipant()
	 * @generated
	 * @ordered
	 */
	protected Participant initiatingParticipant;

	/**
	 * The cached value of the '{@link #getReceivingParticipant() <em>Receiving Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceivingParticipant()
	 * @generated
	 * @ordered
	 */
	protected Participant receivingParticipant;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CltsPackage.Literals.TASK_TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTaskName(String newTaskName) {
		String oldTaskName = taskName;
		taskName = newTaskName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__TASK_NAME, oldTaskName, taskName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getInMessage() {
		return inMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInMessage(Message newInMessage, NotificationChain msgs) {
		Message oldInMessage = inMessage;
		inMessage = newInMessage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__IN_MESSAGE, oldInMessage, newInMessage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInMessage(Message newInMessage) {
		if (newInMessage != inMessage) {
			NotificationChain msgs = null;
			if (inMessage != null)
				msgs = ((InternalEObject)inMessage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CltsPackage.TASK_TRANSITION__IN_MESSAGE, null, msgs);
			if (newInMessage != null)
				msgs = ((InternalEObject)newInMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CltsPackage.TASK_TRANSITION__IN_MESSAGE, null, msgs);
			msgs = basicSetInMessage(newInMessage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__IN_MESSAGE, newInMessage, newInMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message getOutMessage() {
		return outMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOutMessage(Message newOutMessage, NotificationChain msgs) {
		Message oldOutMessage = outMessage;
		outMessage = newOutMessage;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__OUT_MESSAGE, oldOutMessage, newOutMessage);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutMessage(Message newOutMessage) {
		if (newOutMessage != outMessage) {
			NotificationChain msgs = null;
			if (outMessage != null)
				msgs = ((InternalEObject)outMessage).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CltsPackage.TASK_TRANSITION__OUT_MESSAGE, null, msgs);
			if (newOutMessage != null)
				msgs = ((InternalEObject)newOutMessage).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CltsPackage.TASK_TRANSITION__OUT_MESSAGE, null, msgs);
			msgs = basicSetOutMessage(newOutMessage, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__OUT_MESSAGE, newOutMessage, newOutMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant getInitiatingParticipant() {
		if (initiatingParticipant != null && initiatingParticipant.eIsProxy()) {
			InternalEObject oldInitiatingParticipant = (InternalEObject)initiatingParticipant;
			initiatingParticipant = (Participant)eResolveProxy(oldInitiatingParticipant);
			if (initiatingParticipant != oldInitiatingParticipant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT, oldInitiatingParticipant, initiatingParticipant));
			}
		}
		return initiatingParticipant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant basicGetInitiatingParticipant() {
		return initiatingParticipant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitiatingParticipant(Participant newInitiatingParticipant) {
		Participant oldInitiatingParticipant = initiatingParticipant;
		initiatingParticipant = newInitiatingParticipant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT, oldInitiatingParticipant, initiatingParticipant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant getReceivingParticipant() {
		if (receivingParticipant != null && receivingParticipant.eIsProxy()) {
			InternalEObject oldReceivingParticipant = (InternalEObject)receivingParticipant;
			receivingParticipant = (Participant)eResolveProxy(oldReceivingParticipant);
			if (receivingParticipant != oldReceivingParticipant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT, oldReceivingParticipant, receivingParticipant));
			}
		}
		return receivingParticipant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Participant basicGetReceivingParticipant() {
		return receivingParticipant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceivingParticipant(Participant newReceivingParticipant) {
		Participant oldReceivingParticipant = receivingParticipant;
		receivingParticipant = newReceivingParticipant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT, oldReceivingParticipant, receivingParticipant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CltsPackage.TASK_TRANSITION__IN_MESSAGE:
				return basicSetInMessage(null, msgs);
			case CltsPackage.TASK_TRANSITION__OUT_MESSAGE:
				return basicSetOutMessage(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CltsPackage.TASK_TRANSITION__TASK_NAME:
				return getTaskName();
			case CltsPackage.TASK_TRANSITION__IN_MESSAGE:
				return getInMessage();
			case CltsPackage.TASK_TRANSITION__OUT_MESSAGE:
				return getOutMessage();
			case CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT:
				if (resolve) return getInitiatingParticipant();
				return basicGetInitiatingParticipant();
			case CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT:
				if (resolve) return getReceivingParticipant();
				return basicGetReceivingParticipant();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CltsPackage.TASK_TRANSITION__TASK_NAME:
				setTaskName((String)newValue);
				return;
			case CltsPackage.TASK_TRANSITION__IN_MESSAGE:
				setInMessage((Message)newValue);
				return;
			case CltsPackage.TASK_TRANSITION__OUT_MESSAGE:
				setOutMessage((Message)newValue);
				return;
			case CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT:
				setInitiatingParticipant((Participant)newValue);
				return;
			case CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT:
				setReceivingParticipant((Participant)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CltsPackage.TASK_TRANSITION__TASK_NAME:
				setTaskName(TASK_NAME_EDEFAULT);
				return;
			case CltsPackage.TASK_TRANSITION__IN_MESSAGE:
				setInMessage((Message)null);
				return;
			case CltsPackage.TASK_TRANSITION__OUT_MESSAGE:
				setOutMessage((Message)null);
				return;
			case CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT:
				setInitiatingParticipant((Participant)null);
				return;
			case CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT:
				setReceivingParticipant((Participant)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CltsPackage.TASK_TRANSITION__TASK_NAME:
				return TASK_NAME_EDEFAULT == null ? taskName != null : !TASK_NAME_EDEFAULT.equals(taskName);
			case CltsPackage.TASK_TRANSITION__IN_MESSAGE:
				return inMessage != null;
			case CltsPackage.TASK_TRANSITION__OUT_MESSAGE:
				return outMessage != null;
			case CltsPackage.TASK_TRANSITION__INITIATING_PARTICIPANT:
				return initiatingParticipant != null;
			case CltsPackage.TASK_TRANSITION__RECEIVING_PARTICIPANT:
				return receivingParticipant != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (taskName: ");
		result.append(taskName);
		result.append(')');
		return result.toString();
	}

} //TaskTransitionImpl
